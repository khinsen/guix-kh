(define-module (kh packages lisp)
  #:use-module (gnu packages)
  #:use-module (gnu packages lisp)
  #:use-module (gnu packages lisp-xyz)
  #:use-module (gnu packages lisp-check)
  #:use-module (gnu packages maths) ;; for gnuplot in sbcl-common-doc
  #:use-module (gnu packages graphviz) ;; for graphviz in sbcl-common-doc
  #:use-module (gnu packages uml) ;; for plantuml in sbcl-common-doc
  #:use-module (gnu packages freedesktop) ;; for xdg-util in sbcl-clog
  #:use-module (gnu packages web) ;; for esbuild in sbcl-clog
  #:use-module (gnu packages pdf) ;; for poppler in ratp-points-of-sale
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix build-system asdf))

;; CommonDoc and friends, to be submitted to Guix

(define-public sbcl-common-doc
  (let ((commit "6bfcea769abd0b7928b346c8bd96fd0501db3654")
        (revision "0"))
    (package
      (name "sbcl-common-doc")
      (version (git-version "0.2." revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/khinsen/common-doc")
               (commit commit)))
         (file-name (git-file-name "cl-common-doc" version))
         (sha256
          (base32 "15x7lh2q96ds8bdl2y2s0rs341lrag3kiphpbpwiv8yabq50mw98"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-trivial-types
             sbcl-local-time
             sbcl-quri
             sbcl-anaphora
             sbcl-alexandria
             sbcl-closer-mop
             ;; for common-doc-gnuplot and common-doc-include
             sbcl-split-sequence
             gnuplot
             ;; for common-doc-graphviz
             graphviz
             ;; for common-doc-plantuml
             plantuml
             ;; for common-doc-graphviz and common-doc-plantuml
             sbcl-trivial-shell
             ;; for common-doc-split-paragraphs
             sbcl-cl-ppcre))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fix-path-1
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "contrib/gnuplot/gnuplot.lisp"
                 (("\\*gnuplot-command\\* \"gnuplot\"")
                  (string-append "*gnuplot-command* \""
                                 (search-input-file inputs "/bin/gnuplot")
                                 "\"")))))
           (add-after 'unpack 'fix-path-2
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "contrib/graphviz/graphviz.lisp"
                 (("\\*dot-command\\* \"dot\"")
                  (string-append "*dot-command* \""
                                 (search-input-file inputs "/bin/dot")
                                 "\"")))))
           (add-after 'unpack 'fix-path-3
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "contrib/plantuml/plantuml.lisp"
                 (("\"-jar\" \"plantuml.jar\"")
                  (string-append "\"-jar\" \""
                                 (search-input-file inputs
                                                    "/share/java/plantuml.jar")
                                 "\""))))))))
      (home-page "http://commondoc.github.io/")
      (synopsis "Framework for representing and manipulating documents")
      (description
       "CommonDoc is a library of classes for representing structured documents.
It's completely independent of input or output formats, and provides operations
for inspecting and manipulating documents.")
      (license license:expat))))

(define-public cl-common-doc
  (sbcl-package->cl-source-package sbcl-common-doc))

(define-public ecl-common-doc
  (sbcl-package->ecl-package sbcl-common-doc))

(define-public sbcl-common-doc-plump
  (package
    (name "sbcl-common-doc-plump")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/CommonDoc/common-doc-plump")
             (commit "2d9dc9a0d09cea274e3ec961de0b85e7bac39a16")))
       (file-name (git-file-name "cl-common-doc-plump" version))
       (sha256
        (base32 "08h7m4c599rf2kz4wkpbj05441ax0vb3bd88a7dw5x57djf765r6"))))
    (build-system asdf-build-system/sbcl)
    (inputs
     (list sbcl-anaphora
           sbcl-cl-markup
           sbcl-common-doc
           sbcl-plump))
    (home-page "https://github.com/CommonDoc/common-doc-plump")
    (synopsis "Convert Plump documents to CommonDoc and vice versa")
    (description
     "Translate a Plump DOM into a CommonDoc document and back.  Used
by cl-scriba and cl-vertex.")
    (license license:expat)))

(define-public cl-common-doc-plump
  (sbcl-package->cl-source-package sbcl-common-doc-plump))

(define-public ecl-common-doc-plump
  (sbcl-package->ecl-package sbcl-common-doc-plump))

(define-public sbcl-scriba
  (package
    (name "sbcl-scriba")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/CommonDoc/scriba")
             (commit "ce1ab037365200bcca704ac05c306b378051e0b8")))
       (file-name (git-file-name "cl-scriba" version))
       (sha256
        (base32 "1n32bxf3b1cgb7y4015y3vahjgnbw59pi6d08by78pnpa2nx43sa"))))
    (build-system asdf-build-system/sbcl)
    (inputs
     (list sbcl-common-doc
           sbcl-common-doc-plump
           sbcl-esrap
           sbcl-plump-sexp))
    (home-page "http://commondoc.github.io/scriba/docs/overview.html")
    (synopsis "Parser for the Scriba markup format")
    (description
     "Scriba is a markup format inspired by Scribe.  This package provides
a Scriba parser for the CommonDoc framework.")
    (license license:expat)))

(define-public cl-scriba
  (sbcl-package->cl-source-package sbcl-scriba))

(define-public ecl-scriba
  (sbcl-package->ecl-package sbcl-scriba))

(define-public sbcl-plump-tex
  (let ((commit "76cb5819d8bde39a599c150d27a6ed4c14c9d7da")
        (revision "0"))
    (package
      (name "sbcl-plump-tex")
      (version (git-version "0.1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri
          (git-reference
           (url "https://github.com/Shinmera/plump-tex")
           (commit commit)))
         (file-name (git-file-name "cl-plump-tex" version))
         (sha256
          (base32 "1k0cmk5sbn042bx7nxiw0rvsjmgmj221zim1hg23r0485jbx0r3h"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-plump
             sbcl-cl-ppcre))
      (native-inputs
       (list sbcl-fiveam))
      (synopsis "Parser turning TeX-like syntax into a Plump DOM")
      (description
       "A parser that translates TeX tags to DOM nodes.")
      (home-page "https://github.com/Shinmera/plump-tex")
      (license license:zlib))))

(define-public cl-plump-tex
  (sbcl-package->cl-source-package sbcl-plump-tex))

(define-public ecl-plump-tex
  (sbcl-package->ecl-package sbcl-plump-tex))

(define-public sbcl-vertex
  (package
    (name "sbcl-vertex")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/CommonDoc/vertex")
             (commit "5c13132e3b8fea89d463a28f35536ea3a2044d3c")))
       (file-name (git-file-name "cl-vertex" version))
       (sha256
        (base32 "0g3ck1kvp6x9874ffizjz3fsd35a3m4hcr2x5gq9fdql680ic4k2"))))
    (build-system asdf-build-system/sbcl)
    (inputs
     (list sbcl-common-doc
           sbcl-common-doc-plump
           sbcl-plump-tex))
    (home-page "https://github.com/CommonDoc/vertex")
    (synopsis "Parser for the VerTeX markup format")
    (description
     "VerTeX is a markup format inspired by TeX.  This package provides
a VerTeX parser for the CommonDoc framework.")
    (license license:expat)))

(define-public cl-vertex
  (sbcl-package->cl-source-package sbcl-vertex))

(define-public ecl-vertex
  (sbcl-package->ecl-package sbcl-vertex))

(define-public sbcl-parenml
  (package
    (name "sbcl-parenml")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/CommonDoc/parenml")
             (commit "559d402a2d67098705db826b868efe2d08338cb7")))
       (file-name (git-file-name "cl-parenml" version))
       (sha256
        (base32 "0g6s5phinpcfhixgsfqniwxd3kd4bwh78s90ixs2fwk3qjhh9zsb"))))
    (build-system asdf-build-system/sbcl)
    (inputs
     (list sbcl-common-doc
           sbcl-common-doc-plump
           sbcl-esrap
           sbcl-plump))
    (home-page "https://github.com/CommonDoc/parenml")
    (synopsis "Parser for the parenml markup format")
    (description
     "Parenml is a markup format using s-expressions.  This package provides
a parenml parser for the CommonDoc framework.")
    (license license:expat)))

(define-public cl-parenml
  (sbcl-package->cl-source-package sbcl-parenml))

(define-public ecl-parenml
  (sbcl-package->ecl-package sbcl-parenml))

(define-public sbcl-common-html
  (package
    (name "sbcl-common-html")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/CommonDoc/common-html")
             (commit "96987bd9db21639ed55d1b7d72196f9bc58243fd")))
       (file-name (git-file-name "cl-common-html" version))
       (sha256
        (base32 "1i11w4l95nybz5ibnaxrnrkfhch2s9wynqrg6kx6sl6y47khq1xz"))))
    (build-system asdf-build-system/sbcl)
    (inputs
     (list sbcl-alexandria
           sbcl-anaphora
           sbcl-common-doc
           sbcl-plump))
    (home-page "https://github.com/CommonDoc/common-html")
    (synopsis "HTML parser/emitter for CommonDoc")
    (description
     "A HTML parser and emitter for the CommonDoc framework.")
    (license license:expat)))

(define-public cl-common-html
  (sbcl-package->cl-source-package sbcl-common-html))

(define-public ecl-common-html
  (sbcl-package->ecl-package sbcl-common-html))

;;
;; mel-base is unmaintained. This package refers to a bug fix that I applied
;; to the most recent version I could find.
;;
(define-public sbcl-mel-base
  (package
    (name "sbcl-mel-base")
    (version "0.9.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/khinsen/mel-base")
             (commit "1386b7cbc65a0e41913101d778877b067cbcf0a4")))
       (file-name (git-file-name "cl-mel-base" version))
       (sha256
        (base32 "0nk2x0nr0czjmjmz6ki6i54k3lha4ylpkjlmr2w7jh74x4gvp6hf"))))
    (build-system asdf-build-system/sbcl)
    (inputs
     (list sbcl-flexi-streams
           sbcl-cl+ssl
           sbcl-usocket
           sbcl-babel
           sbcl-ironclad
           sbcl-cl-base64
           sbcl-cl-base64))
    (synopsis "Common Lisp mail library")
    (description
     "A versatile mail library for common lisp. Handles POP3,
      IMAP, SMTP, Maildir.")
    (home-page "https://github.com/40ants/mel-base")
    (license license:bsd-2)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CLOG and updated dependencies
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; The update to CLOG has not yet been submitted to Guix.
;; It includes precompiled JavaScript code that needs
;; to be replaced by from-source builds by Guix rules.

(define-public sbcl-clog
  (let ((commit "d3afbd16bc5adf8afd648f942d8085b9ae37f25f")
        (revision "0"))
    (package
      (name "sbcl-clog")
      (version (git-version "2.3" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/rabbibotton/clog")
               (commit commit)))
         (file-name (git-file-name "cl-clog" version))
         (sha256
          (base32 "169hr8jlp02kzh9yirg0rdmprqrwny1vlkbr5c5zxa4a6vlpncz5"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-alexandria
             sbcl-atomics
             sbcl-bordeaux-threads
             sbcl-cl-isaac
             sbcl-cl-pass
             sbcl-cl-ppcre
             sbcl-cl-sqlite
             sbcl-cl-template
             sbcl-clack
             sbcl-closer-mop
             sbcl-colorize
             sbcl-dbi
             sbcl-hunchentoot
             sbcl-lack
             sbcl-mgl-pax
             sbcl-parse-float
             sbcl-print-licenses
             sbcl-quri
             sbcl-trivial-gray-streams
             sbcl-websocket-driver
             xdg-utils))
      (native-inputs
       `(("js-jquery"
          ,(origin
             (method url-fetch)
             (uri "https://code.jquery.com/jquery-3.7.1.js")
             (sha256
              (base32
               "1zicjv44sx6n83vrkd2lwnlbf7qakzh3gcfjw0lhq48b5z55ma3q"))))
         ("js-jquery-ui"
          ,(origin
             (method url-fetch)
             (uri "https://jqueryui.com/resources/download/jquery-ui-1.13.2.zip")
             (sha256
              (base32
               "0xsppsyjqvq8d00dklpj595czwh9fqlkyr1ab6hmk08pdnbn4dzp"))))
         ("esbuild" ,esbuild)))
      (arguments
       `(#:asd-systems '("clog")
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fix-paths
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "source/clog-system.lisp"
                 (("xdg-open") (search-input-file inputs "/bin/xdg-open")))))
           (add-after 'unpack 'replace-bundled-minified-javascript
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke "esbuild"
                       (assoc-ref inputs "js-jquery")
                       "--minify"
                       (string-append "--outfile=static-files/js/jquery.min.js"))))
           )))
      (home-page "https://github.com/rabbibotton/clog")
      (synopsis "Common Lisp Omnificent GUI")
      (description
       "This package provides a Common Lisp web framework for building GUI
applications.  CLOG can take the place, or work along side, most cross platform
GUI frameworks and website frameworks.  The CLOG package starts up the
connectivity to the browser or other websocket client (often a browser embedded
in a native template application).")
      (license license:bsd-3))))

(define-public cl-clog
  (sbcl-package->cl-source-package sbcl-clog))

(define-public ecl-clog
  (sbcl-package->ecl-package sbcl-clog))

(define-public sbcl-clog-docs
  (package
    (inherit sbcl-clog)
    (name "sbcl-clog-docs")
    (inputs
     (modify-inputs (package-inputs sbcl-clog)
       (prepend sbcl-clog
                sbcl-3bmd)))
    (arguments
     '(#:asd-systems '("clog/docs")))))

(define-public cl-clog-docs
  (sbcl-package->cl-source-package sbcl-clog-docs))

(define-public ecl-clog-docs
  (sbcl-package->ecl-package sbcl-clog-docs))

;; The ACE plugin for CLOG - big JavaScript dependency!

(define-public sbcl-clog-ace
  (let ((commit "6fc96bd45855f65d0d09b395c3c658da148fb4a4")
        (revision "2"))
    (package
      (name "sbcl-clog-ace")
      (version (string-append "0.0." revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/khinsen/clog-ace")
               (commit commit)))
         (file-name (git-file-name "cl-clog-ace" version))
         (sha256
          (base32 "1vxr2sma9nrd1807h1ksidxqz2jqn925srpib23xpzf83q5ahgfi"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-clog))
      (arguments
       '(#:asd-systems '("clog-ace")))
      (home-page "https://github.com/rabbibotton/clog-ace")
      (synopsis "CLOG Plugin for the ACE Editor")
      (description "")
      (license license:bsd-3))))

(define-public cl-clog-ace
  (sbcl-package->cl-source-package sbcl-clog-ace))

(define-public ecl-clog-ace
  (sbcl-package->ecl-package sbcl-clog-ace))

(define-public sbcl-clog-terminal
  (let ((commit "304e3308e050abe0184ecc904cd039986e54b34f")
        (revision "1"))
    (package
      (name "sbcl-clog-terminal")
      (version (string-append "0.0." revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/rabbibotton/clog-terminal")
               (commit commit)))
         (file-name (git-file-name "cl-clog-terminal" version))
         (sha256
          (base32 "1pvrja8fvdzqmiqzl23lb7665vcpx9lhwxahns81wlykkyx7cjd5"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-clog))
      (arguments
       '(#:asd-systems '("clog-terminal")))
      (home-page "https://github.com/rabbibotton/clog-terminal")
      (synopsis "CLOG-Terminal plug-in for jQueryTerminal ")
      (description "")
      (license license:bsd-3))))

(define-public cl-clog-terminal
  (sbcl-package->cl-source-package sbcl-clog-terminal))

(define-public ecl-clog-terminal
  (sbcl-package->ecl-package sbcl-clog-terminal))

(define-public sbcl-clog-tools
  (package
    (inherit sbcl-clog)
    (name "sbcl-clog-tools")
    (inputs
     (list sbcl-clog
           sbcl-clog-ace
           sbcl-clog-terminal
           sbcl-s-base64
           sbcl-cl-indentify
           sbcl-definitions
           sbcl-parenscript
           sbcl-plump
           sbcl-slime-swank
           sbcl-trivial-main-thread))
    (arguments
     '(#:asd-systems '("clog/tools")))))

(define-public cl-clog-tools
  (sbcl-package->cl-source-package sbcl-clog-tools))

(define-public ecl-clog-tools
  (sbcl-package->ecl-package sbcl-clog-tools))

;; clog-moldable-inspector depends on a recent CLOG.
;; It doesn't work with sbcl-clog from Guix.

(define-public sbcl-clog-moldable-inspector
  (let ((commit "92cf3ad70a4fc86b58ea96089f0fe21e1e52002a")
        (revision "0"))
    (package
      (name "sbcl-clog-moldable-inspector")
      (version (string-append "0.0.1." revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://codeberg.org/khinsen/clog-moldable-inspector")
               (commit commit)))
         (file-name (git-file-name "clog-moldable-inspector" version))
         (sha256
          (base32 "17zla2rs56wz1prwdlm865qx2glhljwv4qzqfhldzvgcj2f556wm"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-3bmd
             sbcl-alexandria
             sbcl-arrows
             sbcl-cl-base32
             sbcl-clog
             sbcl-clog-ace
             sbcl-cl-str
             sbcl-flexi-streams
             sbcl-fset
             sbcl-html-inspector-views))
      (home-page "https://codeberg.org/khinsen/clog-moldable-inspector")
      (synopsis "")
      (description "")
      (license license:bsd-3))))

(define-public cl-clog-moldable-inspector
  (sbcl-package->cl-source-package sbcl-clog-moldable-inspector))

(define-public ecl-clog-moldable-inspector
  (sbcl-package->ecl-package sbcl-clog-moldable-inspector))

;; html-inspector-views could be submitted to Guix, but
;; it requires clog-moldable-inspector to be useful.

(define-public sbcl-html-inspector-views
  (let ((commit "0fb7cbe1c42b512a45b699df2ba9bb1253f009cd")
        (revision "0"))
    (package
      (name "sbcl-html-inspector-views")
      (version (string-append "0.0.1." revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://codeberg.org/khinsen/html-inspector-views")
               (commit commit)))
         (file-name (git-file-name "html-inspector-views" version))
         (sha256
          (base32 "0c61ddkzb7dypjjy9ff2jw3lcdw80xvx98ql4cm6lr3ln1fybafr"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-cl-who
             sbcl-alexandria
             sbcl-arrows
             sbcl-cl-base64
             sbcl-closer-mop
             sbcl-fset
             sbcl-local-time
             sbcl-s-graphviz
             sbcl-cl-str
             sbcl-slime-swank
             sbcl-trivial-clipboard))
      (arguments
       `(#:asd-systems '("html-inspector-views" "html-inspector-views/standard")))
      (home-page "https://codeberg.org/khinsen/html-inspector-views")
      (synopsis "Defining inspector views in terms of HTML snippets")
      (description "Infrastructure for defining inspector views
in terms of HTML snippets.  Requires an inspector implementation
that can use these views, such as clog-moldable-inspector
(https://codeberg.org/khinsen/clog-moldable-inspector).")
      (license license:bsd-3))))

(define-public cl-html-inspector-views
  (sbcl-package->cl-source-package sbcl-html-inspector-views))

(define-public ecl-html-inspector-views
  (sbcl-package->ecl-package sbcl-html-inspector-views))

;; Inspector views for plump nodes

(define-public sbcl-plump-inspector-views
  (let ((commit "8eeab06d681b64f7bd8c8ad815e029cc81c675d0")
        (revision "0"))
    (package
      (name "sbcl-plump-inspector-views")
      (version (string-append "0.0.1." revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://codeberg.org/khinsen/plump-inspector-views")
               (commit commit)))
         (file-name (git-file-name "plump-inspector-views" version))
         (sha256
          (base32 "186skv5v7nlfzfvmdk9xhg2ggwwz4ygngd4rwyl8dz2jmywha0lm"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-alexandria
             sbcl-arrows
             sbcl-cl-who
             sbcl-html-inspector-views
             sbcl-plump))
      (home-page "https://codeberg.org/khinsen/plump-inspector-views")
      (synopsis "Inspector views for plump nodes")
      (description "Inspector views for plump nodes. See
https://Shinmera.github.io/plump/ for plump.")
      (license license:bsd-3))))

(define-public cl-plump-inspector-views
  (sbcl-package->cl-source-package sbcl-plump-inspector-views))

(define-public ecl-plump-inspector-views
  (sbcl-package->ecl-package sbcl-plump-inspector-views))


;; ratp-points-of-sale

(define-public sbcl-ratp-points-of-sale
  (let ((commit "18b74124a5891d7bd463f745415553d515c58e07")
        (revision "0"))
    (package
      (name "sbcl-ratp-points-of-sale")
      (version (string-append "0.0.1." revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://codeberg.org/khinsen/ratp-points-of-sale")
               (commit commit)))
         (file-name (git-file-name "p-o-s" version))
         (sha256
          (base32 "1b23gw7k7id0pj7i9dr561cgx9h2jr85ykw8dpsnsbmn71p37afj"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-alexandria
             sbcl-arrow-macros
             sbcl-cl-store
             sbcl-cl-str
             sbcl-clog-moldable-inspector
             sbcl-html-inspector-views
             sbcl-jonathan
             sbcl-local-time
             sbcl-parse-float
             sbcl-plump
             sbcl-plump-inspector-views
             sbcl-quri))
      (propagated-inputs
       (list poppler))
      (arguments
       `(#:asd-systems '("ratp-points-of-sale" "ratp-points-of-sale/explore")))
      (home-page "https://codeberg.org/khinsen/ratp-points-of-sale")
      (synopsis "An example of a documented, explorable, reusable dataset")
      (description "A very simple dataset, listing the points of sale for
Paris area metro and bus tickets outside of the Paris region, is used to
illustrate how datasets can be published such that they are documented,
explorable, and reusable.")
      (license license:gpl3+))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Image and CIEL REPL with all packages I use
;; except for my own (which are always loaded
;; from source).
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-public sbcl-kh-dependencies
  (let ((commit "940bb167ccfa15a136b383f63b029f69b4a35db0")
        (revision "6"))
    (package
      (name "sbcl-kh-dependencies")
      (version (string-append "0.1" revision))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://codeberg.org/khinsen/kh-dependencies")
               (commit commit)))
         (file-name (git-file-name "kh-dependencies" version))
         (sha256
          (base32 "13s999wmp30llcvxf1fhdr7cynqpchpr46dm81f7jgzjc1fp0hv9"))))
      (build-system asdf-build-system/sbcl)
      (inputs
       (list sbcl-3bmd
             sbcl-alexandria
             sbcl-arrow-macros
             sbcl-babel
             sbcl-ciel
             sbcl-cl-base64
             sbcl-cl-base32
             sbcl-cl-fad
             sbcl-cl-mime
             sbcl-cl-qprint
             sbcl-cl-store
             sbcl-cl-unicode
             sbcl-cl-who
             sbcl-clog
             sbcl-clog-ace
             sbcl-closer-mop
             sbcl-closure-template
             sbcl-common-doc
             sbcl-common-html
             sbcl-dexador
             sbcl-drakma
             sbcl-eclector
             sbcl-flexi-streams
             sbcl-fset
             sbcl-hunchentoot
             sbcl-jonathan
             sbcl-local-time
             sbcl-mel-base
             sbcl-parse-float
             sbcl-plump
             sbcl-plump-sexp
             sbcl-quickproject
             sbcl-s-graphviz
             sbcl-scriba
             sbcl-slynk
             sbcl-serapeum
             sbcl-cl-str
             sbcl-slime-swank
             sbcl-tooter
             sbcl-trivial-clipboard
             sbcl-trivial-open-browser
             sbcl-vertex
             sbcl-vom))
      (outputs '("out" "image"))
      (arguments
       (list
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'create-asdf-configuration 'build-image
              (lambda* (#:key outputs #:allow-other-keys)
                (build-image
                 (string-append (assoc-ref outputs "image") "/bin/sbcl-kh")
                 outputs
                 #:dependencies '("kh-dependencies")))))))
      (home-page "https://codeberg.org/khinsen/kh-dependencies")
      (synopsis "All my CL dependencies")
      (description "")
      (license license:gpl3+))))

(define-public cl-kh-dependencies
  (sbcl-package->cl-source-package sbcl-kh-dependencies))

(define-public ecl-kh-dependencies
  (let ((pkg (sbcl-package->ecl-package sbcl-kh-dependencies)))
    (package
      (inherit pkg)
      (arguments
       (substitute-keyword-arguments (package-arguments pkg)
         ((#:phases phases)
          `(modify-phases ,phases
             (remove 'build-image))))))))

(define-public sbcl-ciel-repl-kh
  (package
    (inherit sbcl-ciel-repl)
    (name "sbcl-ciel-repl-kh")
    (inputs
     (modify-inputs (package-inputs sbcl-ciel-repl)
       (prepend sbcl-kh-dependencies)))
    (synopsis "CIEL REPL with more built-in packages")))
